<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/carroussel.js"></script>
    <script src="js/menu.js"></script>
    <title><?php echo $this->titre; ?></title>
</head>
<body>
<nav class="navMob navigationMob">   
            <ul>
                <a href="index.php?action=accueil"><li>Accueil</li></a>
                <a href="index.php?action=prestation"><li>Prestation</li></a>
                <a href="index.php?action=ethique"><li>Ethique</li></a>
                <a href="index.php?action=projetrealise"><li>Projet</li></a>
                <a href="index.php?action=contact"><li>Contact</li></a>
            </ul>
        </nav>  
    <header>    
        <h1>Aux Copeaux Ce Bois</h1>
        <button class='navigationMob'>Menu</button>
          
        <nav class="navigation">   
            <ul>
                <a href="index.php?action=accueil"><li>Accueil</li></a>
                <a href="index.php?action=prestation"><li>Prestation</li></a>
                <a href="index.php?action=ethique"><li>Ethique</li></a>
                <a href="index.php?action=projetrealise"><li>Projet</li></a>
                <a href="index.php?action=contact"><li>Contact</li></a>
            </ul>
        </nav>    
    </header>

    <main>
        <?php echo $contenu; ?>
    </main>
    <footer>    
        <p>FOOTER</p>    
    </footer>
</body>
</html>