PROG = {

  //liste des items
  item          : '',
  //ce qui contient les items
  container     : '',
  //taille du container
  ratio         : '',
  //taille des items
  ratioItem     : '',
  //les boutton a ecouter
  element       : '',
  //slide actuel
  currentSlide  : 0,

  init : function() {

    PROG.item         = document.querySelectorAll('.carroussel__item');
    PROG.container    = document.querySelector('.carroussel__container');
    PROG.ratio        = PROG.item.length / 3;
    PROG.ratioItem    = ((100 / 3) / PROG.ratio) + '%';
    PROG.element      = document.querySelectorAll('.button');

    //init de la taille du container
    PROG.container.style.width = (PROG.ratio * 100) + '%';

    //init de la taille des items
    PROG.initWidth();
    // PARA.init();

    //ecoute des items
    PROG.item.forEach(item => item.addEventListener('click', PROG.redirection));
    //ecoute des boutton
    PROG.element.forEach(function(elem){
      elem.addEventListener('click', PROG.caroussel);
    });
  },

  caroussel : function(e) {

    //defilement du carroussel
    if(e.target.attributes.name.nodeValue == 'suivant' && PROG.currentSlide < PROG.item.length - 3){
      PROG.goTo(PROG.currentSlide + 3);
    }

    //defilement du carroussel
    if(e.target.attributes.name.nodeValue == 'precedent' && PROG.currentSlide > 0){
      PROG.goTo(PROG.currentSlide - 3);
    }
  },

  //calcul du deplacment et modification du slide en cours
  goTo : function(index) {
    let deplacement = index * -100 / PROG.item.length;
    PROG.container.style.transform = 'translate3d(' + deplacement + '%, 0, 0)';
    PROG.currentSlide = index;
  },

  redirection : function(evt) {

    console.log(evt);
    // window.location.href="index.php";

  },

  initWidth : function() {

    PROG.item.forEach(item => item.style.width = PROG.ratioItem);
  },
},
window.onload = PROG.init;
