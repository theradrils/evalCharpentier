<?php 
require_once ('../controllers/Vue.php');

class Router {

    private $page;

    public function __construct() {
        if(isset($_GET['action'])){
            $this->page = $_GET['action'];
        }else{
            $this->page = 'accueil';
        }
    }

    public function requete() {

        switch ($this->page) {

            case 'accueil' :
                $vue = new Vue('accueil');
                $vue->generer();
                break;
                
            case 'prestation' :
                $vue = new Vue('prestation');
                $vue->generer();
                break;

            case 'ethique' :
                $vue = new Vue('ethique');
                $vue->generer();
                break;

            case 'projetrealise' :
                $vue = new Vue('projetrealise');
                $vue->generer();
                break;

            case 'contact' :
                $vue = new Vue('contact');
                $vue->generer();
                break;

          
        }
    }
}

?>