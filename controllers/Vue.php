<?php

class Vue {

  private $fichier;
  private $titre;

  public function __construct($action) {
    // Determination du nom du fichier vue a partir de l'action et du constructeur
    $this->fichier  = '../views/' . $action . '.php';    
  }// end construct

  public function generer( $donnees = []) {
     
    $contenu = $this->genererFichier($this->fichier, $donnees);
    
    $vue = $this->genererFichier('../views/gabarit.php', array('contenu' => $contenu, 'titre' => $this->titre));
    echo $vue;
  }//end function

  private function genererFichier($fichier, $donnees) {

      if (file_exists($fichier)) {
          extract($donnees);
          ob_start();
          require $fichier;
          return ob_get_clean();
      }else{
          throw new Exception("fichier $fichier introuvable");
      }//end if
  }// end function
}
?>
